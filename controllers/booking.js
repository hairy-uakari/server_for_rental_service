// eslint-disable-next-line new-cap
const router = require('express').Router();
const Booking = require('../schemas/booking');
const Car = require('../schemas/car');
const {buildQuery, buildMatchQuery} = require('../Util/utils');
const {getToken} = require('../Util/utils');
const jwt = require('jsonwebtoken');

router.get('/', (req, res) => {
  Booking.find({}).then((books) => {
    res.json(books.map((e) => e.toJSON()));
  });
});

router.post('/', async (req, res, next) => {
  if (!req.body.issueDate || !req.body.returnDate) {
    return res.status(422).send('issue and return dates must be provided');
  }
  const token = getToken(req);
  jwt.verify(token, process.env.SECRET, async (err, result) => {
    if (err) {
      return next({origin: 'JWT', err});
    }
    req.body.issueDate = new Date(req.body.issueDate);
    req.body.returnDate = new Date(req.body.returnDate);
    const matchQuery = buildMatchQuery(req.body.model, req.body.capacity);
    const numbers = await Car.aggregate([
      {$match: matchQuery},
      {
        $lookup: {
          from: 'bookings',
          localField: 'number',
          foreignField: 'number',
          as: 'bookings',
        },
      },
      {
        $project: {
          number: 1,
          bookings: {
            $filter: {
              input: '$bookings',
              as: 'booking',
              cond: {
                $or: [
                  {
                    $and: [
                      {$lte: [req.body.issueDate, '$$booking.returnDate']},
                      {$gte: [req.body.issueDate, '$$booking.issueDate']},
                    ],
                  },
                  {
                    $and: [
                      {$lte: [req.body.returnDate, '$$booking.returnDate']},
                      {$gte: [req.body.returnDate, '$$booking.issueDate']},
                    ],
                  },
                  {
                    $and: [
                      {$lte: [req.body.issueDate, '$$booking.issueDate']},
                      {$gte: [req.body.returnDate, '$$booking.issueDate']},
                    ],
                  },
                ],

              },
            },
          },
        },
      },
      {
        $project: {
          number: 1,
          available: {
            $eq: [{$size: '$bookings'}, 0],
          },
        },
      },
      {
        $match: {
          available: true,
        },
      },
    ]);

    if (numbers.length > 0) {
      const booking = new Booking({
        issueDate: req.body.issueDate,
        returnDate: req.body.returnDate,
        customerName: req.body.customerName,
        customerPhone: req.body.customerPhone,
        number: numbers[0].number,
      });
      try {
        const booked = await booking.save();
        res.json(booked.toJSON());
      } catch (err) {
        return next(err);
      }
    } else {
      res.status(404).json({error: 'Car not found'});
    }
  });
});

router.get('/available', async (req, res) => {
  const body = buildQuery(req.query);
  const matchBody = buildMatchQuery(req.query.model, req.query.capacity);
  console.log(body);
  console.log(matchBody);
  const numbers = await Car.aggregate([
    {$match: matchBody},
    {$lookup: {
      from: 'bookings',
      localField: 'number',
      foreignField: 'number',
      as: 'bookings',
    }},
    {$project: {
      number: 1,
      model: 1,
      capacity: 1,
      bookings: {
        $filter: {
          input: '$bookings',
          as: 'booking',
          cond: {
            $or: [
              {$and: [
                {$lte: [body.issueDate, '$$booking.returnDate']},
                {$gte: [body.issueDate, '$$booking.issueDate']},
              ]},
              {$and: [
                {$lte: [body.returnDate, '$$booking.returnDate']},
                {$gte: [body.returnDate, '$$booking.issueDate']},
              ]},
              {$and: [
                {$lte: [body.issueDate, '$$booking.issueDate']},
                {$gte: [body.returnDate, '$$booking.issueDate']},
              ]},
            ],
          },
        },
      },
    }},
    {$project: {
      model: 1,
      capacity: 1,
      number: 1,
      available: {
        $eq: [{$size: '$bookings'}, 0],
      },
    },
    },
    {$match: {
      available: true,
    }},
    {$project: {
      number: 1,
      model: 1,
      capacity: 1,
      _id: 0,
    }},
  ]);

  res.json(numbers);
});

module.exports = {
  router,
};
