// eslint-disable-next-line new-cap
const router = require('express').Router();
const User = require('../schemas/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

router.post('/signup', async (req, res, next) => {
  const rounds = 10;
  const passHash = await bcrypt.hash(req.body.password, rounds);

  const user = new User({
    phoneNumber: req.body.phoneNumber,
    name: req.body.name,
    passwordHash: passHash,
  });

  try {
    const savedUser = await user.save();
    res.json(savedUser.toJSON());
  } catch (err) {
    next(err);
  }
});

router.post('/login', async (req, res) => {
  const user = await User.findOne({phoneNumber: req.body.phoneNumber});
  const passCheck = user === null ? false :
    await bcrypt.compare(req.body.password, user.passwordHash);

  if (user === null || !passCheck) {
    return res.status(401).json({error: 'Invalid phone or pass'});
  }

  const userToken = {
    phoneNumber: user.phoneNumber,
    id: user._id,
  };

  const refreshToken = crypto.randomBytes(30)
      .toString('base64')
      .slice(0, 30);

  const updatedUser = await User.findByIdAndUpdate(user._id,
      {refreshToken: refreshToken}, {new: true});

  console.log(updatedUser);

  const token = jwt.sign(userToken, process.env.SECRET, {expiresIn: '10m'});
  res.send({token, refreshToken: refreshToken});
});

router.post('/refresh', async (req, res) => {
  const user = await User.findOne({...req.body});
  console.log(user);
  if (user === null) {
    return res.status(401).send('Invalid refresh token');
  }

  const userToken = {
    phoneNumber: user.phoneNumber,
    id: user._id,
  };
  const refreshToken = crypto.randomBytes(30)
      .toString('base64')
      .slice(0, 30);

  const updatedUser = await User.findByIdAndUpdate(user._id,
      {refreshToken: refreshToken}, {new: true});
  console.log(updatedUser);
  const token = jwt.sign(userToken, process.env.SECRET, {expiresIn: '10m'});

  res.send({token, refreshToken});
});

module.exports = {
  router,
};
