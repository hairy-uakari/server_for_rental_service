/* eslint-disable new-cap */
// eslint-disable-next-line new-cap
const router = require('express').Router();

const Car = require('../schemas/car');
const mongoose = require('mongoose');
const {getToken} = require('../Util/utils');
const jwt = require('jsonwebtoken');

router.get('/', (req, res) => {
  Car.find({}).then((car) => {
    res.json(car.map((e) => e.toJSON()));
  });
});

router.post('/', async (req, res, next) => {
  const token = getToken(req);
  jwt.verify(token, process.env.SECRET, async (err, result) => {
    if (err) {
      return next({origin: 'JWT', err});
    }
    try {
      const car = new Car(req.body);
      const saved = await car.save();
      res.json(saved.toJSON());
    } catch (err) {
      next(err);
    }
  });
});

router.get('/bookedCars', async (req, res) => {
  const isoDate = new Date().toISOString().split('T')[0];
  const currentDate = new Date(isoDate);
  const bookings = await Car.aggregate([
    {$match: {
      ...req.query,
    }},
    {$lookup: {
      from: 'bookings',
      localField: 'number',
      foreignField: 'number',
      as: 'bookings',
    }},
    {$project: {
      _id: 0,
      bookings: {
        $filter: {
          input: '$bookings',
          as: 'booking',
          cond: {
            // $and: [
            //   {$lte: [currentDate, '$$booking.returnDate']},
            //   {$gte: [currentDate, '$$booking.issueDate']},
            // ],
            $lte: [currentDate, '$$booking.returnDate'],
          },
        },
      },
    }},
    {$unwind: '$bookings'},
    {$project: {
      customerName: '$bookings.customerName',
      customerPhone: '$bookings.customerPhone',
      issueDate: '$bookings.issueDate',
      returnDate: '$bookings.returnDate',
    }},
  ]);
  res.json(bookings);
});

router.put('/:id', async (req, res, next) => {
  const token = getToken(req);
  jwt.verify(token, process.env.SECRET, async (err, result) => {
    if (err) {
      return next({origin: 'JWT', err});
    }
    const isoDate = new Date().toISOString().split('T')[0];
    const currentDate = new Date(isoDate);
    const id = req.params.id.toString();
    const updateInfo = await Car.aggregate([
      {
        $match: {
          _id: mongoose.Types.ObjectId(id),
        },
      },
      {
        $lookup: {
          from: 'bookings',
          localField: 'number',
          foreignField: 'number',
          as: 'bookings',
        },
      },
      {
        $project: {
          bookings: {
            $filter: {
              input: '$bookings',
              as: 'booking',
              cond: {
                $lte: [currentDate, '$$booking.returnDate'],
              },
            },
          },
        },
      },
      {
        $project: {
          canUpdate: {
            $eq: [{$size: '$bookings'}, 0],
          },
        },
      },
    ]);
    if (updateInfo.length > 0 && updateInfo[0].canUpdate === true) {
      const obj = (await Car.findById(id)).toJSON();
      const updatedObj = {
        ...obj,
        ...req.body,
      };
      const updated = await Car.findByIdAndUpdate(id, updatedObj,
          {new: true});
      res.json(updated.toJSON());
    } else {
      next(new Error('Car is booked, cannot update'));
    }
  });
});

router.delete('/:id', async (req, res, next) => {
  const token = getToken(req);
  jwt.verify(token, process.env.SECRET, async (err, result) => {
    if (err) {
      return next({origin: 'JWT', err});
    }
    const isoDate = new Date().toISOString().split('T')[0];
    const currentDate = new Date(isoDate);
    const id = req.params.id.toString();
    const deleteInfo = await Car.aggregate([
      {
        $match: {
          _id: mongoose.Types.ObjectId(id),
        },
      },
      {
        $lookup: {
          from: 'bookings',
          localField: 'number',
          foreignField: 'number',
          as: 'bookings',
        },
      },
      {
        $project: {
          bookings: {
            $filter: {
              input: '$bookings',
              as: 'booking',
              cond: {
                $lte: [currentDate, '$$booking.returnDate'],
              },
            },
          },
        },
      },
      {
        $project: {
          canDelete: {
            $eq: [{$size: '$bookings'}, 0],
          },
        },
      },
    ]);
    if (deleteInfo.length > 0 && deleteInfo[0].canDelete === true) {
      await Car.findByIdAndDelete(id);
      res.status(204).end();
    } else {
      next(new Error('Car is booked, cannot delete'));
    }
  });
});

module.exports = {
  router,
};
