const mongoose = require('mongoose');
const moment = require('moment');

const requiredDate = {
  type: Date,
  required: true,
};

const bookingSchema = new mongoose.Schema({
  customerName: {
    type: String,
    required: true,
  },
  customerPhone: {
    type: String,
    required: true,
    unique: true,
    match: /[0-9]{5}/,
  },
  issueDate: requiredDate,
  returnDate: requiredDate,
  number: {
    type: String,
    required: true,
  },
  expire_at: {type: Date,
    default: moment(Date.now()).add(90, 'days').toDate(), expires: 5},
});

bookingSchema.pre('validate', function(next) {
  if (moment().isAfter(this.issueDate)) {
    next(new Error('Invalid issue date'));
  } else if (moment(this.issueDate).isAfter(this.returnDate)) {
    next(new Error('Issue date should be before return date'));
  } else if (moment(this.returnDate).diff(this.issueDate, 'days') > 20) {
    next(new Error('Car cannot be rented for so long!'));
  } else {
    next();
  }
});

bookingSchema.pre('save', function(next) {
  if (this.isModified('expire_at')) {
    next(new Error('expire field is read only!'));
  } else {
    next();
  }
});

bookingSchema.set('toJSON', {
  transform: (document, returnedObj) => {
    returnedObj.id = returnedObj._id.toString();
    delete returnedObj._id;
    delete returnedObj.expire_at;
    delete returnedObj.__v;
  },
});

module.exports = mongoose.model('Booking', bookingSchema);
